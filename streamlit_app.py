import streamlit as st
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options
import csv
import time


def run_selenium_app(user_input):
    chrome_options = Options()
    chrome_options.add_argument('--headless=new')  # Run Chrome in headless mode
    # chrome_options.add_argument("--disable-gpu")  # Disable GPU acceleration
    driver = webdriver.Chrome(executable_path=r"C:\Users\MuhammadAwais\Downloads\chromedriver_win32 (1)\chromedriver.exe", options=chrome_options)

    # Navigate to Google
    driver.get("https://www.google.com")

    # Find the search box and enter a query
    search_box = driver.find_element(By.NAME, "q")
    search_box.send_keys(user_input)
    search_box.send_keys(Keys.RETURN)

    # Wait for the page to load
    time.sleep(1)

    result_links = driver.find_elements(By.XPATH, "//div/a/h3")

    data = []  # List to store the URL and price data

    for link in result_links:
        try:
            link_url = link.get_attribute("href")
            if link_url is None:
                parent_element = link.find_element(By.XPATH, "..")
                link_url = parent_element.get_attribute("href")

            link.click()
            time.sleep(1)

            try:
                price = driver.find_element(By.XPATH, "//*[contains(text(), 'Rs.')]")
                price_text = price.text
                data.append({"URL": link_url, "Price": price_text})
            except:
                pass

            # Simulate going back
            driver.execute_script('window.history.go(-1)')
        except:
            pass

    # Close the webdriver
    driver.quit()

    return data


def main():
    st.set_page_config(page_title="Selenium Streamlit App", layout="wide", initial_sidebar_state="collapsed")

    # App header
    st.title("Selenium Streamlit App")
    st.header("Google Search Scraper")

    # Sidebar
    st.sidebar.title("Dashboard")
    user_input = st.sidebar.text_input("Enter your query")

    # Main content
    if st.sidebar.button("Run Selenium") and user_input:
        with st.spinner("Running Selenium..."):
            scraped_data = run_selenium_app(user_input)
            if len(scraped_data) > 0:
                st.success("Data scraped successfully!")
                st.subheader("Scraped Data:")
                for item in scraped_data:
                    st.write("URL:", item["URL"])
                    st.write("Price:", item["Price"])
                    st.write("---")
            else:
                st.warning("No data found.")
    elif user_input:
        st.info("Click 'Run Selenium' to start the scraping process.")


if __name__ == "__main__":
    main()

